apiVersion: v1
kind: Namespace
metadata:
  name: volcano-system
---
apiVersion: v1
kind: Namespace
metadata:
  name: volcano-monitoring
---
# Source: volcano/templates/scheduler.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: volcano-scheduler-configmap
  namespace: volcano-system
data:
  volcano-scheduler.conf: |
    actions: "enqueue, allocate, backfill"
    tiers:
    - plugins:
      - name: priority
      - name: gang
      - name: conformance
      - name: volcano-npu_v6.0.RC1_linux-x86_64
    - plugins:
      - name: drf
      - name: predicates
      - name: proportion
      - name: nodeorder
      - name: binpack
    configurations:
      - name: init-params
        arguments: {"grace-over-time":"900","presetVirtualDevice":"true","nslb-version":"1.0","shared-tor-num":"2",
    "useClusterInfoManager":"true","super-pod-size": "48", "reserve-nodes": "2"}

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: volcano-scheduler
  namespace: volcano-system
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: volcano-scheduler
rules:
  - apiGroups: ["apiextensions.k8s.io"]
    resources: ["customresourcedefinitions"]
    verbs: ["create", "get", "list", "watch", "delete"]
  - apiGroups: ["batch.volcano.sh"]
    resources: ["jobs"]
    verbs: ["get", "list", "watch", "update", "delete"]
  - apiGroups: ["batch.volcano.sh"]
    resources: ["jobs/status"]
    verbs: ["update", "patch"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["create", "list", "watch", "update", "patch"]
  - apiGroups: [""]
    resources: ["services"]
    verbs: ["get"]
  - apiGroups: [""]
    resources: ["pods", "pods/status"]
    verbs: ["create", "get", "list", "watch", "update", "patch", "bind", "updateStatus", "delete"]
  - apiGroups: [""]
    resources: ["pods/binding"]
    verbs: ["create"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["list", "watch", "update"]
  - apiGroups: [""]
    resources: ["persistentvolumes"]
    verbs: ["list", "watch"]
  - apiGroups: [""]
    resources: ["namespaces"]
    verbs: ["list", "watch"]
  - apiGroups: [""]
    resources: ["resourcequotas"]
    verbs: ["list", "watch"]
  - apiGroups: [ "storage.k8s.io" ]
    resources: ["storageclasses", "csinodes", "csidrivers", "csistoragecapacities"]
    verbs: [ "list", "watch" ]
  - apiGroups: [""]
    resources: ["nodes"]
    verbs: ["list", "watch"]
  - apiGroups: ["policy"]
    resources: ["poddisruptionbudgets"]
    verbs: ["list", "watch"]
  - apiGroups: ["scheduling.k8s.io"]
    resources: ["priorityclasses"]
    verbs: ["get", "list", "watch"]
  - apiGroups: ["scheduling.incubator.k8s.io", "scheduling.volcano.sh"]
    resources: ["queues"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: ["scheduling.incubator.k8s.io", "scheduling.volcano.sh"]
    resources: ["podgroups"]
    verbs: ["list", "watch", "update"]
  - apiGroups: ["nodeinfo.volcano.sh"]
    resources: ["numatopologies"]
    verbs: ["get", "list", "watch", "delete"]
  - apiGroups: [""]
    resources: ["configmaps"]
    verbs: ["get", "create", "delete", "update","list","watch"]
  - apiGroups: ["apps"]
    resources: ["daemonsets", "replicasets", "statefulsets", "deployments"]
    verbs: ["list", "watch", "get"]
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: volcano-scheduler-role
subjects:
  - kind: ServiceAccount
    name: volcano-scheduler
    namespace: volcano-system
roleRef:
  kind: ClusterRole
  name: volcano-scheduler
  apiGroup: rbac.authorization.k8s.io

---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: volcano-scheduler
  namespace: volcano-system
  labels:
    app: volcano-scheduler
spec:
  replicas: 1
  selector:
    matchLabels:
      app: volcano-scheduler
  template:
    metadata:
      labels:
        app: volcano-scheduler
      annotations:
        seccomp.security.alpha.kubernetes.io/pod: runtime/default
    spec:
      nodeSelector:
        masterselector: dls-master-node
      serviceAccount: volcano-scheduler
      securityContext:
        fsGroup: 9000
      containers:
        - name: volcano-scheduler
          image: volcanosh/vc-scheduler:v1.4.0
          command: ["/bin/ash"]
          args: ["-c", "umask 027; /vc-scheduler
                  --scheduler-conf=/volcano.scheduler/volcano-scheduler.conf
                  --plugins-dir=plugins
                  --logtostderr=false
                  --log_dir=/var/log/mindx-dl/volcano-scheduler
                  --log_file=/var/log/mindx-dl/volcano-scheduler/volcano-scheduler.log
                  -v=2 2>&1"]
          imagePullPolicy: "IfNotPresent"
          resources:
            requests:
              memory: 300Mi
              cpu: 500m
            limits:
              memory: 300Mi
              cpu: 500m
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 9000
            runAsGroup: 9000
            capabilities:
              drop: ["ALL"]
          volumeMounts:
            - name: scheduler-config
              mountPath: /volcano.scheduler
              readOnly: true
            - name: scheduler-config-log
              mountPath: /var/log/mindx-dl/volcano-scheduler
            - name: localtime
              mountPath: /etc/localtime
              readOnly: true
      volumes:
        - name: scheduler-config
          configMap:
            defaultMode: 0004
            name: volcano-scheduler-configmap
        - name: scheduler-config-log
          hostPath:
            path: /var/log/mindx-dl/volcano-scheduler
            type: Directory
        - name: localtime
          hostPath:
            path: /etc/localtime
---
apiVersion: v1
kind: Service
metadata:
  annotations:
    prometheus.io/path: /metrics
    prometheus.io/port: "8080"
    prometheus.io/scrape: "true"
  name: volcano-scheduler-service
  namespace: volcano-system
spec:
  ports:
    - port: 8080
      protocol: TCP
      targetPort: 8080
  selector:
    app: volcano-scheduler
  type: ClusterIP
---
# Source: volcano/templates/controllers.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: volcano-controllers
  namespace: volcano-system

---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: volcano-controllers
rules:
  - apiGroups: ["apiextensions.k8s.io"]
    resources: ["customresourcedefinitions"]
    verbs: ["create", "get", "list", "watch", "delete"]
  - apiGroups: ["batch.volcano.sh"]
    resources: ["jobs"]
    verbs: ["get", "list", "watch", "update", "delete"]
  - apiGroups: ["batch.volcano.sh"]
    resources: ["jobs/status", "jobs/finalizers"]
    verbs: ["update", "patch"]
  - apiGroups: ["bus.volcano.sh"]
    resources: ["commands"]
    verbs: ["get", "list", "watch", "delete"]
  - apiGroups: [""]
    resources: ["events"]
    verbs: ["create", "list", "watch", "update", "patch"]
  - apiGroups: [""]
    resources: ["pods"]
    verbs: ["create", "get", "list", "watch", "update", "bind", "delete"]
  - apiGroups: [""]
    resources: ["persistentvolumeclaims"]
    verbs: ["get", "list", "watch", "create"]
  - apiGroups: [""]
    resources: ["services"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: [""]
    resources: ["configmaps"]
    verbs: ["get", "list", "watch", "create", "delete", "update"]
  - apiGroups: [""]
    resources: ["secrets"]
    verbs: ["get", "list", "watch", "create", "delete", "update"]
  - apiGroups: ["scheduling.incubator.k8s.io", "scheduling.volcano.sh"]
    resources: ["podgroups", "queues", "queues/status"]
    verbs: ["get", "list", "watch", "create", "delete", "update"]
  - apiGroups: ["scheduling.k8s.io"]
    resources: ["priorityclasses"]
    verbs: ["get", "list", "watch", "create", "delete"]
  - apiGroups: ["networking.k8s.io"]
    resources: ["networkpolicies"]
    verbs: ["get", "create", "delete"]

---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: volcano-controllers-role
subjects:
  - kind: ServiceAccount
    name: volcano-controllers
    namespace: volcano-system
roleRef:
  kind: ClusterRole
  name: volcano-controllers
  apiGroup: rbac.authorization.k8s.io

---
kind: Deployment
apiVersion: apps/v1
metadata:
  name: volcano-controllers
  namespace: volcano-system
  labels:
    app: volcano-controller
spec:
  replicas: 1
  selector:
    matchLabels:
      app: volcano-controller
  template:
    metadata:
      labels:
        app: volcano-controller
      annotations:
        seccomp.security.alpha.kubernetes.io/pod: runtime/default
    spec:
      nodeSelector:
        masterselector: dls-master-node
      serviceAccount: volcano-controllers
      securityContext:
        fsGroup: 9000
      containers:
        - name: volcano-controllers
          image: volcanosh/vc-controller-manager:v1.4.0
          imagePullPolicy: "IfNotPresent"
          command: [ "/bin/ash" ]
          args: [ "-c", "umask 027;/vc-controller-manager
                     --logtostderr=false
                     --log_dir=/var/log/mindx-dl/volcano-controller
                     --log_file=/var/log/mindx-dl/volcano-controller/volcano-controller.log
                     -v=4 2>&1" ]
          resources:
            requests:
              memory: 3Gi
              cpu: 500m
            limits:
              memory: 3Gi
              cpu: 500m
          securityContext:
            allowPrivilegeEscalation: false
            readOnlyRootFilesystem: true
            runAsUser: 9000
            runAsGroup: 9000
            capabilities:
              drop: ["ALL"]
          volumeMounts:
            - name: device-volcanocontroller
              mountPath: /var/log/mindx-dl/volcano-controller
            - name: localtime
              mountPath: /etc/localtime
              readOnly: true
      volumes:
        - name: device-volcanocontroller
          hostPath:
            path: /var/log/mindx-dl/volcano-controller
            type: Directory
        - name: localtime
          hostPath:
            path: /etc/localtime
---
# Source: volcano/templates/batch_v1alpha1_job.yaml
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: jobs.batch.volcano.sh
spec:
  group: batch.volcano.sh
  names:
    kind: Job
    plural: jobs
    shortNames:
      - vcjob
      - vj
  scope: Namespaced
  validation:
    openAPIV3Schema:
      type: object
      properties:
        apiVersion:
          description: 'APIVersion defines the versioned schema of this representation
            of an object. Servers should convert recognized schemas to the latest
            internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
          type: string
        kind:
          description: 'Kind is a string value representing the REST resource this
            object represents. Servers may infer this from the endpoint the client
            submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
          type: string
        metadata:
          type: object
        spec:
          description: Specification of the desired behavior of a cron job, including
            the minAvailable
          properties:
            runningEstimate:
              description: Running Estimate is a user running duration estimate for job Default to nil.
              type: string
            volumes:
              description: The volumes for Job
              items:
                properties:
                  volumeClaim:
                    description: VolumeClaim defines the PVC used by the VolumeMount.
                    type: object
                  mountPath:
                    description: Path within the container at which the volume should be mounted.
                      Must not contain ':'.
                    type: string
                  volumeClaimName:
                    description: The name of the volume claim.
                    type: string
                type: object
                required:
                  - mountPath
              type: array
            minAvailable:
              description: The minimal available pods to run for this Job
              format: int32
              type: integer
            policies:
              description: Specifies the default lifecycle of tasks
              items:
                properties:
                  action:
                    description: The action that will be taken to the PodGroup according
                      to Event. One of "Restart", "None". Default to None.
                    type: string
                  event:
                    description: The Event recorded by scheduler; the controller takes
                      actions according to this Event.
                    type: string
                  events:
                    description: The Events recorded by scheduler; the controller takes
                      actions according to this Events.
                    type: array
                    items:
                      type: string
                  timeout:
                    description: Timeout is the grace period for controller to take
                      actions. Default to nil (take action immediately).
                    type: object
                type: object
              type: array
            schedulerName:
              description: SchedulerName is the default value of `tasks.template.spec.schedulerName`.
              type: string
            plugins:
              description: Enabled task plugins when creating job.
              type: object
            tasks:
              description: Tasks specifies the task specification of Job
              items:
                properties:
                  dependsOn:
                    description: Specifies the tasks that this task depends on.
                    properties:
                      iteration:
                        description: This field specifies that when there are multiple
                          dependent tasks, as long as one task becomes the specified
                          state, the task scheduling is triggered or all tasks must
                          be changed to the specified state to trigger the task
                          scheduling
                        type: string
                      name:
                        description: Indicates the name of the tasks that this task
                          depends on, which can depend on multiple tasks
                        items:
                          type: string
                        type: array
                    type: object
                  name:
                    description: Name specifies the name of tasks
                    type: string
                  policies:
                    description: Specifies the lifecycle of task
                    items:
                      properties:
                        action:
                          description: The action that will be taken to the PodGroup
                            according to Event. One of "Restart", "None". Default
                            to None.
                          type: string
                        event:
                          description: The Event recorded by scheduler; the controller
                            takes actions according to this Event.
                          type: string
                        events:
                          description: The Events recorded by scheduler; the controller takes
                            actions according to this Events.
                          type: array
                          items:
                            type: string
                        timeout:
                          description: Timeout is the grace period for controller
                            to take actions. Default to nil (take action immediately).
                          type: object
                      type: object
                    type: array
                  replicas:
                    description: Replicas specifies the replicas of this TaskSpec
                      in Job
                    format: int32
                    type: integer
                  template:
                    description: Specifies the pod that will be created for this TaskSpec
                      when executing a Job
                    type: object
                type: object
              type: array
            queue:
              description: The name of the queue on which job should been created
              type: string
            maxRetry:
              description: The limit for retrying submiting job, default is 3
              format: int32
              type: integer
          type: object
        status:
          description: Current status of Job
          properties:
            conditions:
              description: Which conditions caused the current job state.
              items:
                description: JobCondition contains details for the current condition
                  of this job.
                properties:
                  lastTransitionTime:
                    description: Last time the condition transitioned from one phase
                      to another.
                    format: date-time
                    type: string
                  status:
                    description: Status is the new phase of job after performing
                      the state's action.
                    type: string
                required:
                  - status
                type: object
              type: array
            succeeded:
              description: The number of pods which reached phase Succeeded.
              format: int32
              type: integer
            failed:
              description: The number of pods which reached phase Failed.
              format: int32
              type: integer
            minAvailable:
              description: The minimal available pods to run for this Job
              format: int32
              type: integer
            pending:
              description: The number of pending pods.
              format: int32
              type: integer
            running:
              description: The number of running pods.
              format: int32
              type: integer
            version:
              description: Job's current version
              format: int32
              type: integer
            retryCount:
              description: The number that volcano retried to submit the job.
              format: int32
              type: integer
            controlledResources:
              description: All of the resources that are controlled by this job.
              type: object
              additionalProperties:
                type: string
            runningDuration:
              description: the length of time from job running to complete
              type: string
            state:
              description: Current state of Job.
              properties:
                message:
                  description: Human-readable message indicating details about last
                    transition.
                  type: string
                phase:
                  description: The phase of Job
                  type: string
                reason:
                  description: Unique, one-word, CamelCase reason for the condition's
                    last transition.
                  type: string
                lastTransitionTime:
                  description: The time of last state transition.
                  format: date-time
                  type: string
              type: object
          type: object
  version: v1alpha1
  subresources:
    status: {}
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []

---
# Source: volcano/templates/bus_v1alpha1_command.yaml
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: commands.bus.volcano.sh
spec:
  group: bus.volcano.sh
  names:
    kind: Command
    plural: commands
  scope: Namespaced
  validation:
    openAPIV3Schema:
      type: object
      properties:
        action:
          description: Action defines the action that will be took to the target object.
          type: string
        apiVersion:
          description: 'APIVersion defines the versioned schema of this representation
            of an object. Servers should convert recognized schemas to the latest
            internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#resources'
          type: string
        kind:
          description: 'Kind is a string value representing the REST resource this
            object represents. Servers may infer this from the endpoint the client
            submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/api-conventions.md#types-kinds'
          type: string
        message:
          description: Human-readable message indicating details of this command.
          type: string
        metadata:
          type: object
        reason:
          description: Unique, one-word, CamelCase reason for this command.
          type: string
        target:
          description: TargetObject defines the target object of this command.
          type: object
  version: v1alpha1
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []

---
# Source: volcano/templates/scheduling_v1beta1_podgroup.yaml
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: podgroups.scheduling.volcano.sh
spec:
  group: scheduling.volcano.sh
  names:
    kind: PodGroup
    plural: podgroups
    shortNames:
      - pg
      - podgroup-v1beta1
  scope: Namespaced
  validation:
    openAPIV3Schema:
      properties:
        apiVersion:
          type: string
        kind:
          type: string
        metadata:
          type: object
        spec:
          properties:
            minMember:
              format: int32
              type: integer
            queue:
              type: string
            priorityClassName:
              type: string
          type: object
        status:
          properties:
            succeeded:
              format: int32
              type: integer
            failed:
              format: int32
              type: integer
            running:
              format: int32
              type: integer
          type: object
      type: object
  version: v1beta1

---
# Source: volcano/templates/scheduling_v1beta1_queue.yaml
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: queues.scheduling.volcano.sh
spec:
  group: scheduling.volcano.sh
  names:
    kind: Queue
    plural: queues
    shortNames:
      - q
      - queue-v1beta1
  scope: Cluster
  validation:
    openAPIV3Schema:
      properties:
        apiVersion:
          type: string
        kind:
          type: string
        metadata:
          type: object
        spec:
          properties:
            weight:
              format: int32
              type: integer
            capability:
              type: object
          type: object
        status:
          properties:
            state:
              type: string
            unknown:
              format: int32
              type: integer
            pending:
              format: int32
              type: integer
            running:
              format: int32
              type: integer
            inqueue:
              format: int32
              type: integer
          type: object
      type: object
  version: v1beta1
  subresources:
    status: {}
---
# Source: volcano/templates/nodeinfo_v1alpha1_numatopologies.yaml
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.6.0
  creationTimestamp: null
  name: numatopologies.nodeinfo.volcano.sh
spec:
  group: nodeinfo.volcano.sh
  names:
    kind: Numatopology
    listKind: NumatopologyList
    plural: numatopologies
    shortNames:
      - numatopo
    singular: numatopology
  scope: Cluster
  versions:
    - name: v1alpha1
      schema:
        openAPIV3Schema:
          description: Numatopology is the Schema for the Numatopologies API
          properties:
            apiVersion:
              description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
              type: string
            kind:
              description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
              type: string
            metadata:
              type: object
            spec:
              description: Specification of the numa information of the worker node
              properties:
                cpuDetail:
                  additionalProperties:
                    description: CPUInfo is the cpu topology detail
                    properties:
                      core:
                        type: integer
                      numa:
                        type: integer
                      socket:
                        type: integer
                    type: object
                  description: Specifies the cpu topology info Key is cpu id
                  type: object
                numares:
                  additionalProperties:
                    description: ResourceInfo is the sets about resource capacity and
                      allocatable
                    properties:
                      allocatable:
                        type: string
                      capacity:
                        type: integer
                    type: object
                  description: Specifies the numa info for the resource Key is resource
                    name
                  type: object
                policies:
                  additionalProperties:
                    type: string
                  description: Specifies the policy of the manager
                  type: object
                resReserved:
                  additionalProperties:
                    type: string
                  description: Specifies the reserved resource of the node Key is resource
                    name
                  type: object
              type: object
          type: object
      served: true
      storage: true
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
